%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Beispiel der Zugbewegungen-Routinen für UM-L´
%
% U. Meyer, Okt. 2008, Feb. 2015
%
%
% Benötigt board.pl
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
:- dynamic
	fehler/2.
fehler(nein,weiss).	% Schwarz beginnt das Spiel, s.u.!!

:- [board].

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%% BEGIN-NEW-CODE %%%%%%%%%%%%%%%%%%%%%

% #################### MISCELLANEOUS ######################

% *********************************************************
% ******************** member(M,List) *********************
% *********************************************************
% **** Proofs if M is inside List                      ****
% ****                                                 ****
% **** example :                                       ****
% ****    member(4,[1,2,a,b,4]).                        ****
% *********************************************************

member(M,[M|_]).
member(M,[_|NewList]) :- member(M,NewList).

% *********************************************************
% ************ concatLists(List1,List2,Result) ************
% *********************************************************
% **** Concats List1 and List2 and returns them into   ****
% **** Result.                                         ****
% ****                                                 ****
% **** example :                                       ****
% ****    concatLists([a1,a2,a3],[b1,b2,b3,b4],R).     ****
% *********************************************************

concatLists([],Result,Result).
concatLists([Head|Body],List2,Result) :- concatLists(Body,[Head|List2],Result).


% #################### PROGRAM_FLOW #######################

% *********************************************************
% ********************* start(color) **********************
% *********************************************************
% **** start(Color) is an alternative function to      ****
% **** start(). It automatically calculates and inserts****
% **** a turn if Color has to make his turn.           ****
% **** If its not Color who has to make his turn, the  ****
% **** user has to enter the turn into the keyboard,   ****
% **** as it was in original function start().         ****
% ****                                                 ****
% **** example :                                       ****
% ****    start(schwarz).                              ****
% *********************************************************
start(Color) :-  retractall(currentColor(_,_)),
                 assertz(currentColor(weiss)),
	         retractall(aiColor(_)),
        	 assertz(aiColor(Color)),
                 initBrett,
                 retract(fehler(_,_)),
                 assertz(fehler(nein,weiss)),
                 dialog.
          

% *********************************************************
% *********************** dialog() ************************
% *********************************************************
% **** dialog() is a replacement of the orignal        ****
% **** function dialog(). The original function is now ****
% **** called dialogOld() and is no longer used.       ****
% **** dialog() has exactly the same functionality as  ****
% **** dialogOld(), but it automatically calculates    ****
% **** and inserts a turn, if Color (which was set     ****
% **** by start(Color)) has to turn.                   ****
% **** If its not this Color which has to make its     ****
% **** turn, the user has to enter the next turn into  ****
% **** the keyboard, as it was in dialogOld().         ****
% **** dialog() uses getNextTurn(...) for calculating  ****
% **** and setting a turn.                             ****
% ****                                                 ****
% **** example :                                       ****
% ****    dialog.                                      ****
% *********************************************************
dialog :-  color(Color),
           currentColor(CurrentColor),
           (Color \== CurrentColor;fail),
           retractall(currentColor(_)),
           assertz(currentColor(Color)),
           schreibeBrett,
           write(Color),
           write(' am Zug  ==> '),
           calcPossibleTurnsForColor(Color,Result),
           write('Moegliche Zuege : '),write(Result),
           write('\n ==> '),
           getNextTurn(_,_,Zugfolge),
           ziehen(Color,Zugfolge),
           staticEvaluate(EvalW,EvalS),
           write('Bewertung des Zuges fuer S :'), write(EvalS), write('\n'),
           write('Bewertung des Zuges fuer W :'), write(EvalW), write('\n'),
           dialog.

% *********************************************************
% ********************* color(Result) *********************
% *********************************************************
% **** alternative function to farbe, which calculates ****
% **** alternating schwarz and weiß.                   **** 
% ****                                                 ****
% **** example :                                       ****
% ****    color(Color),                                ****
% ****    currentColor(CurrentColor),                  ****
% ****    (Color \== CurrentColor;fail),               ****
% ****    retractall(currentColor(_)),                 ****
% ****    assertz(currentColor(Color)),                ****
% *********************************************************

color(schwarz).
color(weiss).


% ********************************************************* 
% ****** getNextTurn(AIColor,CurrentColor,NextTurn) ******* 
% ********************************************************* 
% **** TODO                                            ****
% ****                                                 ****
% **** example :                                       ****
% ****    getNextTurn(schwarz,schwarz,NextTurn)        **** 
% *********************************************************

%getNextTurn(AIColor,AIColor,NextTurn) :- AIColor == schwarz, calcNextTurn(AIColor,NextTurn) , write(NextTurn).
%getNextTurn(AIColor,AIColor,NextTurn) :- AIColor == weiss, calcNextTurn(AIColor,NextTurn) , write(NextTurn).
%getNextTurn(AIColor,Color,NextTurn) :- AIColor \== Color, read(NextTurn).
getNextTurn(_,_,NextTurn) :- read(NextTurn).


% ********************************************************* 
% ****************** calcPositionList() ******************* 
% ********************************************************* 
% **** calcPositionList() creates and asserts two lists**** 
% **** L1 and L2 within the asserted facts             ****
% **** positionList(w,L1) and positionList(s,L2)       **** 
% **** L1 is a list of all fields, where w is on top.  **** 
% **** L2 is a list of all fields, where s is on top.  ****
% **** This can be used for evaluation and to not need ****
% **** iterate all fields during calcPossibleTurns().  ****
% ****                                                 **** 
% **** example :                                       **** 
% ****    calcPositionList.                            **** 
% ****    positionList(w,L1).                          ****
% ****    positionList(s,L2).                          ****
% **** example :                                       ****
% ****    calcPositionList(w).			       ****	
% ****    positionList(w,L).                           ****
% *********************************************************

calcPositionList :- retractall(positionList(_,_)), calcPositionList(w), calcPositionList(s).
calcPositionList(Color) :- retractall(positionList(Color,_)),calcPositionList(Color,[a4,a6,b3,b5,b7,c2,c4,c6,c8,d1,d3,d5,d7,d9,e2,e4,e6,e8,f3,f5,f7,g4,g6],[]).
calcPositionList(Color,[],Result) :- assertz(positionList(Color,Result)).
calcPositionList(Color,[Field|FieldList],PositionList) :- brett(Field,[Color|_]),calcPositionList(Color,FieldList,[Field|PositionList]).
calcPositionList(s,[Field|FieldList],PositionList) :- brett(Field,[r|_]),calcPositionList(s,FieldList,[Field|PositionList]).
calcPositionList(w,[Field|FieldList],PositionList) :- brett(Field,[g|_]),calcPositionList(w,FieldList,[Field|PositionList]).
calcPositionList(Color,[_|FieldList],PositionList) :- calcPositionList(Color,FieldList,PositionList).

% *********************************************************
% ******** calcPossibleTurnsForColor(Color,Result) ********
% *********************************************************
% **** Calculates the currently possible turns for     ****
% **** Color and returns them into Result.             ****
% ****                                                 ****
% **** For to speed up this function, calcPositionList ****
% **** must not be called every time again, the        ****
% **** program has to store a current positionList     ****
% **** and has to keep it up to date at every move and ****
% **** jump. This allows the program to not always     ****
% **** iterate all fields and search for positions.    ****
% ****                                                 ****
% **** example :                                       ****
% ****    calcPossibleTurnsForColor(schwarz,Result)    ****
% *********************************************************

calcPossibleTurnsForColor(schwarz,Result) :- calcPossibleTurnsForColor(s,Result).
calcPossibleTurnsForColor(weiss,Result) :- calcPossibleTurnsForColor(w,Result).

calcPossibleTurnsForColor(Color,Result) :- calcPositionList(Color), positionList(Color,PositionList),
                                           calcPossibleTurnsForColor(PositionList,[],Result).
calcPossibleTurnsForColor([],Result,Result).
calcPossibleTurnsForColor([Position|PositionList],TurnsList,Result) :- calcPossibleTurns(Position,PossibleTurns),
                                                                       mergePositionAndTurns(Position,PossibleTurns,MergedPositionAndTurns),
								       concatLists(MergedPositionAndTurns,TurnsList,ConcatedList),
                                                                       calcPossibleTurnsForColor(PositionList,ConcatedList,Result).


mergePositionAndTurns(Position,TurnsList,Result) :- mergePositionAndTurns(Position,TurnsList,[],Result).
mergePositionAndTurns(_,[],Result,Result).
mergePositionAndTurns(Position,[Turn|TurnsList],MergedList,Result) :- concat(Position,Turn,MergedItem),
                                                                      mergePositionAndTurns(Position,TurnsList,[MergedItem|MergedList],Result).

% *********************************************************
% ************* calcPossibleTurns(Field,Result) ***********
% *********************************************************
% **** Creates a list of all possible turns (jumps and ****
% **** moves) from Field, asserts this list and adds   ****
% **** returns this list by Result.                    ****
% ****                                                 ****
% **** example :                                       ****
% ****    calcPossibleTurns(e4,T)                      ****
% *********************************************************


calcPossibleTurns(Field,Result) :- retractall(possibleTurns(Field,_)),
                                   assertz(possibleTurns(Field,[])),
                                   not(calcPossibleMoves(Field)),!,
                                   not(calcPossibleJumps(Field)),!,
                                   possibleTurns(Field,Result).


% *********************************************************
% **************** calcPossibleMoves(Field) ***************
% *********************************************************
% **** Creates a list of all possible moves, from      ****
% **** Field and adds this list to the asserted fact   ****
% **** possibleTurns(Field,List)                       ****
% ****                                                 ****
% **** example :                                       ****
% ****    calcPossibleMoves(e4)                        ****
% *********************************************************


calcPossibleMoves(Field) :- (move(Field,Turn);move(Turn,Field)),
                            brett(Turn,[]),
                            checkPromotionAllowsMove(Field,Turn),
                            possibleTurns(Field,Temp),
		            retract(possibleTurns(Field,_)),
                            assertz(possibleTurns(Field,[Turn|Temp])),
                            fail.

% *********************************************************
% **************** calcPossibleJumps(Field) ***************
% *********************************************************
% **** Creates a list of all possible jumps, from      ****
% **** Field and adds this list to the asserted fact   ****
% **** possibleTurns(Field,List)                       ****
% ****                                                 ****
% **** example :                                       ****
% ****    calcPossibleJumps(e4)                        ****
% *********************************************************

calcPossibleJumps(Field) :- (jump(Field,Middle,Turn);jump(Turn,Middle,Field)), % get all theoretically possible jumps
                            brett(Turn,[]),            % check if new field is empty
                            checkPromotionAllowsJump(Field,Turn), 
                            brett(Field,[Color|_]),    % getCurrentColor
                            brett(Middle,[Oppo|_]),    % check if the color you want to jail is your opponent
                            opponent(Color,Oppo),      %    -||-
                            possibleTurns(Field,Temp),
                            retract(possibleTurns(Field,_)),
                            assertz(possibleTurns(Field,[Turn|Temp])),
                            fail.

% *********************************************************
% ****** checkPromotionAllowsMove(FromField,ToField) ****** 
% *********************************************************
% **** Checks if the color on fromField, depending on  **** 
% **** his promotion theoretically is allowed to move  ****
% **** in the direction of FromField to ToField.       ****
% **** It does NOT! check, if the move is possible.    ****
% **** TODO : promotion kicks promotion                ****
% ****                                                 ****
% **** example :                                       ****
% ****    checkPromotionAllowsMove(e4,d5)              ****
% *********************************************************

checkPromotionAllowsMove(From,To) :- brett(From,[s|_]), move(To,From).
checkPromotionAllowsMove(From,To) :- brett(From,[r|_]), (move(To,From);move(From,To)).
checkPromotionAllowsMove(From,To) :- brett(From,[w|_]), move(From,To).
checkPromotionAllowsMove(From,To) :- brett(From,[g|_]), (move(From,To);move(To,From)).  

% *********************************************************
% ****** checkPromotionAllowsJump(FromField,ToField) ******
% *********************************************************
% **** Checks if the color on fromField, depending on  ****
% **** his promotion theoretically is allowed to jump  ****
% **** in the direction of FromField to ToField.       ****
% **** It does NOT! check, if the jump is possible.    ****
% **** TODO : promotion kicks promotion                ****
% ****                                                 ****
% **** example :                                       ****
% ****    checkPromotionAllowsJump(e4,c6)              ****
% *********************************************************

checkPromotionAllowsJump(From,To) :- brett(From,[s|_]), jump(To,_,From).
checkPromotionAllowsJump(From,To) :- brett(From,[r|_]), (jump(To,_,From);jump(From,_,To)).
checkPromotionAllowsJump(From,To) :- brett(From,[w|_]), jump(From,_,To).
checkPromotionAllowsJump(From,To) :- brett(From,[g|_]), (jump(From,_,To);jump(To,_,From)).

% *********************************************************
% ************** calcNextTurn(Color,Result) ***************
% *********************************************************
% **** TODO                                            ****
% ****                                                 ****
% **** example:                                        ****
% ****    calcNextTurn(schwarz,Result).                ****
% *********************************************************

calcNextTurn(schwarz,NextTurn) :- NextTurn = e2d3.
calcNextTurn(weiss,NextTurn) :- NextTurn = c2d1. 

% ################# EVALUATION-METHODS ####################

% NOTES : for to speed up evaluation, evaluate only one
%         color, not both


% *********************************************************
% ******* staticEvaluate(ResultW,ResultS,FieldList) *******
% *********************************************************
% **** Calculates on how many fields player w is on    ****
% **** top (ResultW) and on how many fields player s   ****
% **** is on top (ResultS).                            ****
% **** Only the fields inside FieldList are included   ****
% **** in the calculation.                             ****
% **** staticEvaluate is at least factor 30 faster     ****
% **** than dynamicEvaluate.                           ****
% ****                                                 ****
% **** example :                                       ****
% ****    staticEvaluate(W,S,[a4,a6,b7,d1,e2]).        ****
% ****                                                 ****
% ****    if you want to evaluate over all fields use  ****                                       
% ****    staticEvaluate(W,S).                         ****  
% *********************************************************

staticEvaluate(ResultW,ResultS) :- staticEvaluate(ResultW,ResultS,[a4,a6,b3,b5,b7,c2,c4,c6,c8,d1,d3,d5,d7,d9,e2,e4,e6,e8,f3,f5,f7,g4,g6]).
staticEvaluate(ResultW,ResultS,FieldList) :- staticEvaluate(ResultW,ResultS,FieldList,0,0).
staticEvaluate(ResultW,ResultS,[],ResultW,ResultS).
staticEvaluate(ResultW,ResultS,[Field|FieldList],CounterW,CounterS) :- brett(Field,[w|_]),NewCounterW is CounterW + 1, staticEvaluate(ResultW,ResultS,FieldList,NewCounterW,CounterS).
staticEvaluate(ResultW,ResultS,[Field|FieldList],CounterW,CounterS) :- brett(Field,[s|_]),NewCounterS is CounterS + 1, staticEvaluate(ResultW,ResultS,FieldList,CounterW,NewCounterS).

staticEvaluate(ResultW,ResultS,[Field|FieldList],CounterW,CounterS) :- brett(Field,[r|_]),NewCounterS is CounterS + 1, staticEvaluate(ResultW,ResultS,FieldList,CounterW,NewCounterS).
staticEvaluate(ResultW,ResultS,[Field|FieldList],CounterW,CounterS) :- brett(Field,[g|_]),NewCounterW is CounterW + 1, staticEvaluate(ResultW,ResultS,FieldList,NewCounterW,CounterS).

staticEvaluate(ResultW,ResultS,[_|FieldList],CounterW,CounterS) :- staticEvaluate(ResultW,ResultS,FieldList,CounterW,CounterS).


% *********************************************************
% ************ dynamicEvaluate(ResultW,ResultS) ***********
% *********************************************************
% **** Dynamically creates a list of all declared      ****
% **** fields and calculates on how many fields        ****
% **** player w is on top (ResultW) and on how many    ****
% **** fields player s is on top (ResultS).            ****
% **** dynamicEvaluate is at least factor 30 slower    ****
% **** than staticEvaluate.                            ****
% ****                                                 ****
% **** example :                                       ****
% ****    dynamicEvaluate(W,S).                        ****
% *********************************************************
dynamicEvaluate(ResultW,ResultS) :- dynamicEvaluate(ResultW,ResultS,[]).
dynamicEvaluate(ResultW,ResultS,[K|FieldList]) :-  K==g6, staticEvaluate(ResultW,ResultS,[K|FieldList]). 
dynamicEvaluate(ResultW,ResultS,FieldList) :- brett(Field,_), not(member(Field,FieldList)), dynamicEvaluate(ResultW,ResultS,[Field|FieldList]). 

% ############ SAVE-FIELD-DURING-CALCULATION ##############

% *********************************************************
% ********************** saveFields() *********************
% *********************************************************
% **** Saves all brett()-data to savedField()-data     ****
% **** This can be used to save the original position  ****
% **** of all players, during the program is           ****
% **** calculating brett()-data for the next rounds.   ****
% **** After all calculations loadFields has to be     ****
% **** used to get the original positions before all   ****
% **** calculations.                                   ****
% ****                                                 ****
% **** example :                                       ****
% ****    saveFields.                                  ****
% *********************************************************

saveFields :- retractall(savedField(_,_)),
              saveFields([a4,a6,b3,b5,b7,c2,c4,c6,c8,d1,d3,d5,d7,d9,e2,e4,e6,e8,f3,f5,f7,g4,g6]).
saveFields([]).
saveFields([Field|FieldList]) :- assertField(Field), saveFields(FieldList).
assertField(Field) :- brett(Field,ColorList),assertz(savedField(Field,ColorList)).

% *********************************************************
% ********************** loadFields() *********************
% *********************************************************
% **** Loads all savedField()-data back to brett()-data****
% **** This can be used to get back the original       ****
% **** positions of all players, after the program     ****
% **** has calculated brett()-data for the next rounds.****
% ****                                                 ****
% **** example :                                       ****
% ****    loadFields.                                  ****
% *********************************************************

loadFields :- loadFields([a4,a6,b3,b5,b7,c2,c4,c6,c8,d1,d3,d5,d7,d9,e2,e4,e6,e8,f3,f5,f7,g4,g6]).
loadFields([]).
loadFields([Field|FieldList]) :- retract(brett(Field,_)),savedField(Field,ColorList), assertz(brett(Field,ColorList)), loadFields(FieldList).


% %%%%%%%%%%%%%%%%%%%%%% END-NEW-CODE %%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Mögliche Züge aus der Sicht von Weiss (von -- nach)
move(a4,b3).
move(a4,b5).
move(a6,b5).
move(a6,b7).
move(b3,c2).
move(b3,c4).
move(b5,c4).
move(b5,c6).
move(b7,c6).
move(b7,c8).
move(c2,d1).
move(c2,d3).
move(c4,d3).
move(c4,d5).
move(c6,d5).
move(c6,d7).
move(c8,d7).
move(c8,d9).
move(d1,e2).
move(d3,e2).
move(d3,e4).
move(d5,e4).
move(d5,e6).
move(d7,e6).
move(d7,e8).
move(d9,e8).
move(e2,f3).
move(e4,f3).
move(e4,f5).
move(e6,f5).
move(e6,f7).
move(e8,f7).
move(f3,g4).
move(f5,g4).
move(f5,g6).
move(f7,g6).

% Mögliche Sprünge aus der Sicht von Weiss (von -- über -- nach)
jump(a4,b3,c2).
jump(a4,b5,c6).
jump(a6,b5,c4).
jump(a6,b7,c8).
jump(b3,c2,d1).
jump(b3,c4,d5).
jump(b5,c4,d3).
jump(b5,c6,d7).
jump(b7,c6,d5).
jump(b7,c8,d9).
jump(c2,d3,e4).
jump(c4,d3,e2).
jump(c4,d5,e6).
jump(c6,d5,e4).
jump(c6,d7,e8).
jump(c8,d7,e6).
jump(d1,e2,f3).
jump(d3,e4,f5).
jump(d5,e4,f3).
jump(d5,e6,f7).
jump(d7,e6,f5).
jump(d9,e8,f7).
jump(e2,f3,g4).
jump(e4,f5,g6).
jump(e6,f5,g4).
jump(e8,f7,g6).

start :-
	retract(fehler(_,_)),
	assertz(fehler(nein,weiss)),
	initBrett,
        dialog.
        
dialogOld :-
	farbe(Farbe),
	schreibeBrett,
	write(Farbe),
	write(' am Zug  ==> '),
	read(Zugfolge),
	ziehen(Farbe,Zugfolge).
farbe(F) :- fehler(ja,F).
farbe(schwarz) :- fehler(nein,weiss).
farbe(weiss) :- fehler(nein,schwarz).
farbe(F) :- farbe(F).

ziehen(_,halt) :-
        write('Spiel beendet.'),nl,
        halt.
ziehen(_,init) :-
        write('Alles auf Anfang!'),nl,
        start.
ziehen(Farbe,Zug) :-
	atom_length(Zug,4),
	zug(Farbe,Zug),
	retract(fehler(_,_)),asserta(fehler(nein,Farbe)). %,
	%!,fail.
ziehen(Farbe,_) :-
	nl,write('Ungültige Eingabe!'),nl,nl,
	retract(fehler(_,_)),asserta(fehler(ja,Farbe)),
	fail.
zug(Farbe,Zug) :-
	sub_atom(Zug,0,2,_,FeldA),
	sub_atom(Zug,2,2,_,FeldZ),
	FeldA \== FeldZ,
	selbst(Farbe,FeldA,Kopf),  % ist das Ausgangsfeld mit einer eigenen Farbe besetzt?
	brett(FeldZ,[]),           % ist das Zielfeld leer?
	test(Kopf,FeldA,FeldZ),
	!.

selbst(schwarz,Feld,s) :-
	brett(Feld,[s|_]).
selbst(schwarz,Feld,r) :-
	brett(Feld,[r|_]).
selbst(weiss,Feld,w) :-
	brett(Feld,[w|_]).
selbst(weiss,Feld,g) :-
	brett(Feld,[g|_]).

test(s,A,Z) :-
        jump(Z,M,A),
        brett(M,[Oppo|Jailed]),
        opponent(s,Oppo),
        doJump(A,M,Oppo,Jailed,Z).
test(s,A,Z) :-
        move(Z,A),
        doMove(A,Z).
test(w,A,Z) :-
        jump(A,M,Z),
        brett(M,[Oppo|Jailed]),
        opponent(w,Oppo),
        doJump(A,M,Oppo,Jailed,Z).
test(w,A,Z) :-        
        move(A,Z),
        doMove(A,Z).

% ATTENTION NEW CODE FOR PROMOTION MOVEMENT
test(r,A,Z) :-
        (jump(Z,M,A);jump(A,M,Z)),
        brett(M,[Oppo|Jailed]),
        opponent(r,Oppo),
        doJump(A,M,Oppo,Jailed,Z).

test(r,A,Z) :- 
        (move(Z,A);move(A,Z)),
        doMove(A,Z).

test(g,A,Z) :- 
       (jump(Z,M,A);jump(A,M,Z)),
        brett(M,[Oppo|Jailed]),
        opponent(g,Oppo),
        doJump(A,M,Oppo,Jailed,Z).

test(g,A,Z) :- 
        (move(Z,A);move(A,Z)),
        doMove(A,Z).



opponent(g,r).
opponent(r,g).
opponent(g,s).
opponent(r,w).
% ATTENTION NEW CODE FOR PROMOTION MOVEMENT

opponent(w,s).
opponent(w,r).
opponent(s,w).
opponent(s,g).

degradiere(g,w).
degradiere(r,s).
degradiere(X,X).     % degradiere nicht

% Felder, auf denen gewöhnliche Steine befördert werden
promotion(a4,s,r).
promotion(a6,s,r).
promotion(g4,w,g).
promotion(g6,w,g).
promotion(_,X,X).    % befördere nicht

doJump(X,M,O,J,Y) :-
        retract(brett(X,[Kopf|S])),
        assertz(brett(X,[])),
        retract(brett(M,_)),
        assertz(brett(M,J)),
        retract(brett(Y,_)),
        promotion(Y,Kopf,Offz),
        degradiere(O,G),
        append([Offz|S],[G],New),
        assertz(brett(Y,New)).
doMove(X,Y) :-
        retract(brett(X,[Kopf|S])),
        assertz(brett(X,[])),
        retract(brett(Y,_)),
        promotion(Y,Kopf,Offz),
        assertz(brett(Y,[Offz|S])).
        
demo :-
	schreibeBrett(schwarz),write(e3d4),nl,
	selbst(schwarz,e3,KopfS1),
	e3 \== d4,
	testMove(schwarz,KopfS1,e3,d4),
	move(e3,d4),
	schreibeBrett(weiss),write(c5e3),nl,
	selbst(weiss,c5,KopfW),
	c5 \== e3,
	testJump(weiss,KopfW,c5,e3,M1),
	jump(c5,M1,e3),
	schreibeBrett(schwarz),write(f2d4),nl,
	selbst(schwarz,f2,KopfS2),
	f2 \== d4,
	testJump(schwarz,KopfS2,f2,d4,M2),
	jump(f2,M2,d4),
	schreibeBrett(weiss),!.
