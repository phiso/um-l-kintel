miniMax(Depth,Evaluation,Turn) :- 
  retractall(counter(_)),
  asserta(counter(1)),
  retractall(topLevelDepth(_)),
  asserta(topLevelDepth(Depth)),
  retractall(bestTurn(_)),
  asserta(bestTurn([])),
  max(Depth,-10000,10000,Evaluation,_),
  bestTurn(Turn), retractall(bestTurn(_)),
  retractall(topLevelDepth(_)).

max(0,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta)  :-
  rndEvaluate(Evaluation),
  shift(1), write('Calculated evalutation : '), write(Evaluation), write('\n'),
  checkEvaluationIsBestMin(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,_,0,_).

max(Depth,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :- 
  shift(Depth), write('PreviousAlpha : '),write(PreviousAlpha),write(' PreviousBeta : '),write(PreviousBeta), write('\n'),
  generateList([Turn|TurnList]),
  iterateListMax(Depth,[Turn|TurnList],_,PreviousAlpha,PreviousBeta,TempNewAlpha,TempNewBeta), 
  swapAlphaBeta(Depth,TempNewAlpha,TempNewBeta,NewAlpha,NewBeta),
  shift(Depth), write('NewAlpha : '),write(NewAlpha),write(' NewBeta : '),write(NewBeta), write('\n').

min(0,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :-
  rndEvaluate(Evaluation),
  shift(1), write('Calculated evalutation : '), write(Evaluation), write('\n'),
  checkEvaluationIsBestMax(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,_,0,_).

min(Depth,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :-
  shift(Depth), write('PreviousAlpha : '),write(PreviousAlpha),write(' PreviousBeta : '),write(PreviousBeta), write('\n'),
  generateList(L),
  iterateListMin(Depth,L,_,PreviousAlpha,PreviousBeta,TempNewAlpha,TempNewBeta), 
  swapAlphaBeta(Depth,TempNewAlpha,TempNewBeta,NewAlpha,NewBeta),
  shift(Depth), write('NewAlpha : '),write(NewAlpha),write(' NewBeta : '),write(NewBeta), write('\n').

iterateListMax(_,[],false,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta).
iterateListMax(Depth,[Turn|TurnList],Break,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :-
  shift(Depth),write('DoMaxTurn : '), write(Turn), write('\n'),
  NewDepth is Depth - 1,
  min(NewDepth,PreviousAlpha,PreviousBeta,TempNewAlpha1,_),
  shift(Depth),write('UndoMaxTurn : '), write(Turn), write('\n'),
  checkEvaluationIsBestMax(TempNewAlpha1,PreviousAlpha,PreviousBeta,TempNewAlpha2,TempNewBeta2,Break,Depth,Turn),
  (Break = true,NewBeta = TempNewAlpha2, NewAlpha = TempNewBeta2; 
   iterateListMax(Depth,TurnList,_,TempNewAlpha2,TempNewBeta2,NewAlpha,NewBeta)).

iterateListMin(_,[],false,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta).
iterateListMin(Depth,[Turn|TurnList],Break,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :-
  shift(Depth),write('DoMinTurn : '), write(Turn), write('\n'),
  NewDepth is Depth - 1,
  max(NewDepth,PreviousAlpha,PreviousBeta,_,TempNewBeta1),
  shift(Depth),write('UndoMinTurn : '), write(Turn), write('\n'),
  checkEvaluationIsBestMin(TempNewBeta1,PreviousAlpha,PreviousBeta,TempNewAlpha2,TempNewBeta2,Break,Depth,Turn),
  (Break = true,NewBeta = TempNewAlpha2, NewAlpha = TempNewBeta2 ; 
   iterateListMin(Depth,TurnList,_,TempNewAlpha2,TempNewBeta2,NewAlpha,NewBeta)).

swapAlphaBeta(Depth,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta) :- topLevelDepth(Depth).
swapAlphaBeta(_,PreviousAlpha,PreviousBeta,PreviousBeta,PreviousAlpha).

checkEvaluationIsBestMax(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,Break,Depth,Turn) :-
  Evaluation > PreviousAlpha,
  NewAlpha = Evaluation,
  NewBeta = PreviousBeta,
  checkSaveTurn(Depth,Turn),
  Break = false.

checkEvaluationIsBestMax(_,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta,false,_,_).

checkEvaluationIsBestMin(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,Break,Depth,Turn) :-
  Evaluation < PreviousBeta,
  NewBeta = Evaluation,
  NewAlpha = PreviousAlpha,
  checkSaveTurn(Depth,Turn),
  Break = false.

checkEvaluationIsBestMin(_,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta,false,_,_).

checkBreakIterationMax(_,false).

checkBreakIterationMin(_,false).

checkSaveEvaluationMax(_,true). % save nothing if break is already true

checkSaveEvaluationMin(_,true). % save nothing if break is already true

checkSaveTurn(Depth,Turn) :- topLevelDepth(Depth), retractall(bestTurn(_)), asserta(bestTurn(Turn)).
checkSaveTurn(_,_). % save nothing if depth != topLevelDepth

generateList([-a,-b]).

%shift(Count) :- notrace,topLevelDepth(Depth), Temp is Depth - Count, Temp == 0,trace.
%shift(Count) :- notrace,write(' '), NewCount is Count + 1, shift(NewCount),trace. 
shift(Count) :- topLevelDepth(Depth), Temp is Depth - Count, Temp == 0.
shift(Count) :- write(' '), NewCount is Count + 1, shift(NewCount). 

%rndEvaluate(Result) :- random(L), Result is round(L * 10).
rndEvaluate(Result) :- counter(Count), retractall(counter(_)),
                       NewCount is Count + 1,asserta(counter(NewCount)),
                       testVal(Count,Result).

% depth 2, branching factor 2
%testVal(1,-1).
%testVal(2,14).
%testVal(3,1).
%testVal(4,2).

% depth 3, branching factor 2
%testVal(1,-10).
%testVal(2,-6).
%testVal(3,-13).
%testVal(4,-5).
%testVal(5,-5).
%testVal(6,9).
%testVal(7,8).
%testVal(8,-16).

% depth 3, branching factor 2
%testVal(1,-7).
%testVal(2,-5).
%testVal(3,7).
%%testVal(4,-1). will be a break
%testVal(4,2).
%testVal(5,16).
%testVal(6,-18).
%testVal(7,-6).

% depth 3, branching factor 2
%testVal(1,1).
%testVal(2,0).
%testVal(3,-18).
%testVal(4,-15).
%testVal(5,6).
%testVal(6,-7).
%testVal(7,10).
%testVal(8,-6). % will be a break

% depth 3, branching factor 2
%testVal(1,16).
%testVal(2,20).
%testVal(3,15).
%testVal(4,-16).
%testVal(5,15).
%testVal(6,-6).
%testVal(7,-20). % will be a break
%testVal(8,17). % will be a break

% depth 3, branching factor 2
%testVal(1,-6).
%testVal(2,-5).
%testVal(3,11).
%%testVal(4,1). % will be a break
%testVal(4,-17).
%testVal(5,-8).
%testVal(6,15). % will be a break
%testVal(7,10). % will be a break

% depth 3, branching factor 2
%testVal(1,-15).
%testVal(2,-9).
%testVal(3,9).
%%testVal(4,11). % will be a break
%testVal(4,-20).
%testVal(5,-3).
%testVal(6,-1). 
%testVal(7,5). % will be a break

% depth 3, branching factor 2
testVal(1,-14).
testVal(2,-5).
testVal(3,4).
testVal(4,11).
testVal(5,-7).
testVal(6,1).
testVal(7,-8).
testVal(8,10).

% depth 3, branching factor 3
%testVal(1,1).
%testVal(2,-18).
%testVal(3,-12).
%testVal(4,9).
%%testVal(5,10). % will be a break
%%testVal(5,-5). % will be a break
%testVal(5,4).
%%testVal(6,14). % will be a break
%%testVal(6,-14).% will be a break 
%testVal(6,0).
%testVal(7,-8).
%testVal(8,1). 
%%testVal(9,20). % will be a break
%%testVal(9,-2). % will be a break
%%testVal(9,-5). % will be a break
%%testVal(9,-13). % will be a break
%%testVal(9,7). % will be a break
%%testVal(9,-1). % will be a break 
%testVal(9,19). 
%testVal(10,-3). 
%testVal(11,-8). 
%testVal(12,14). 
%testVal(13,0). 
%testVal(14,20). 
%testVal(15,13). 
%testVal(16,-13). 
%testVal(17,10). 

