initTest :- initBrett,asserta(usedTime(0)),asserta(rating(w,90)),asserta(rating(s,90)),asserta(aiColor(schwarz)),asserta(nonAiColor(weiss)).

% *********************************************************
% *********** alphaBeta(Depth,Evaluation,Turn) ************
% *********************************************************
% **** alphaBeta implements the alphaBeta-algorithm on ****
% **** the UM-L-game, which is an Laska-alike game.    ****
% **** alphaBeta uses the the functions max,min        ****
% **** iterateListMax and iterateListMin for looping,  ****
% **** the function generateList for getting all       ****
% **** possible turns, the functions doTurn and        ****
% **** undoTurn for executing and reverting a turn and ****
% **** evaluate for rating a leaf-node.             ****
% **** Depth means the size of how many branches the   ****
% **** tree of possible turns will be calculated.      ****
% **** alphaBeta returns the rating of the chosen Turn ****
% **** into Evaluation and the Turn into Turn.         ****
% ****                                                 ****
% **** example :                                       ****
% ****    alphaBeta(5,Evaluation,Turn).                ****
% *********************************************************
alphaBeta(Depth,Evaluation,Turn) :- 
  retractall(counter(_)),
  asserta(counter(1)),
  retractall(topLevelDepth(_)),
  asserta(topLevelDepth(Depth)),
  retractall(bestTurn(_)),
  asserta(bestTurn([])),
  max(Depth,-10000,10000,Evaluation,_),
  bestTurn(Turn), retractall(bestTurn(_)),
  retractall(topLevelDepth(_)).

% *********************************************************
% **max(Depth,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta)*
% *********************************************************
% **** if Depth > 0 then                               ****    
% ****   max is generating a new List of possible Turns****
% ****   then max iterates this list and calls for     ****
% ****   every turn inside the generated List the min- ****
% ****   function.                                     ****
% ****   if Beta =< Alpha then                         ****
% ****     the recursion stops                         ****  
% **** if Depth == 0 then                              ****
% ****   the recursion stops                           ****
% ****   the board is evaluated / rated                ****
% ****   if Beta =< Alpha then                         ****
% ****     the recursion stops                         ****
% ****                                                 ****
% **** example :                                       ****
% ****    max(3,-10000,10000,A,B).                     ****
% *********************************************************
max(0,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta)  :-
  evaluate(Evaluation),
  %shift(1), write('Calculated evalutation : '), write(Evaluation), write('\n'),
  checkEvaluationIsBestMin(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,_,0,_).

max(Depth,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :- 
  %shift(Depth), write('PreviousAlpha : '),write(PreviousAlpha),write(' PreviousBeta : '),write(PreviousBeta), write('\n'),
  aiColor(AIColor),
  generateList(AIColor,L),
  iterateListMax(Depth,L,_,PreviousAlpha,PreviousBeta,TempNewAlpha,TempNewBeta), 
  swapAlphaBeta(Depth,TempNewAlpha,TempNewBeta,NewAlpha,NewBeta).
  %shift(Depth), write('NewAlpha : '),write(NewAlpha),write(' NewBeta : '),write(NewBeta), write('\n').

% *********************************************************
% **min(Depth,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta)*
% *********************************************************
% **** if Depth > 0 then                               ****    
% ****   min is generating a new List of possible Turns****
% ****   then min iterates this list and calls for     ****
% ****   every turn inside the generated List the max- ****
% ****   function.                                     ****
% ****   if Beta =< Alpha then                         ****
% ****     the recursion stops                         ****  
% **** if Depth == 0 then                              ****
% ****   the recursion stops                           ****
% ****   the board is evaluated / rated                ****
% ****   if Beta =< Alpha then                         ****
% ****     the recursion stops                         ****
% ****                                                 ****
% **** example :                                       ****
% ****    min(3,-10000,10000,A,B).                     ****
% *********************************************************
min(0,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :-
  evaluate(Evaluation),
  %shift(1), write('Calculated evalutation : '), write(Evaluation), write('\n'),
  checkEvaluationIsBestMax(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,_,0,_).

min(Depth,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :-
  %shift(Depth), write('PreviousAlpha : '),write(PreviousAlpha),write(' PreviousBeta : '),write(PreviousBeta), write('\n'),
  nonAiColor(NonAIColor),
  generateList(NonAIColor,L),
  iterateListMin(Depth,L,_,PreviousAlpha,PreviousBeta,TempNewAlpha,TempNewBeta), 
  swapAlphaBeta(Depth,TempNewAlpha,TempNewBeta,NewAlpha,NewBeta).
  %shift(Depth), write('NewAlpha : '),write(NewAlpha),write(' NewBeta : '),write(NewBeta), write('\n').

% **************************************************************************************
% ** iterateListMax(Depth,TurnList,Break,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) **
% **************************************************************************************
% **** iterateListMax() iterates all Turns inside the TurnList. During the iteration****    
% **** iterateListMax() updates Alpha and Beta if checkEvaluationIsBestMax() finds  ****
% **** better Alpha and Beta values.                                                ****
% **** The iteration stops if no Turns are in the list anymore or if                ****
% **** checkBreakIteration() returns Break = "true".                                ****
% **** After finishing the iteration, iterateListMax() returns the new Alpha and    ****
% **** Beta - values                                                                ****
% ****                                                                              ****
% **** example :                                                                    ****
% ****  iterateListMax(2,[b2c3,b3c4],Break,-10000,10000,NewAlpha,NewBeta).          ****
% **************************************************************************************
iterateListMax(_,[],false,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta).
iterateListMax(Depth,[Turn|TurnList],Break,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :-
  %shift(Depth),write('DoMaxTurn : '), write(Turn), write('\n'),
  doTurn(Turn),
  NewDepth is Depth - 1,
  min(NewDepth,PreviousAlpha,PreviousBeta,TempNewAlpha1,_),
  %shift(Depth),write('UndoMaxTurn : '), write(Turn), write('\n'),
  undoTurn(Turn),
  checkEvaluationIsBestMax(TempNewAlpha1,PreviousAlpha,PreviousBeta,TempNewAlpha2,TempNewBeta2,Break,Depth,Turn),
  (Break = true,NewBeta = TempNewAlpha2, NewAlpha = TempNewBeta2; 
   iterateListMax(Depth,TurnList,_,TempNewAlpha2,TempNewBeta2,NewAlpha,NewBeta)).

% **************************************************************************************
% ** iterateListMin(Depth,TurnList,Break,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) **
% **************************************************************************************
% **** iterateListMin() iterates all Turns inside the TurnList. During the iteration****    
% **** iterateListMin() updates Alpha and Beta if checkEvaluationIsBestMin() finds  ****
% **** better Alpha and Beta values.                                                ****
% **** The iteration stops if no Turns are in the list anymore or if                ****
% **** checkBreakIteration() returns Break = "true".                                ****
% **** After finishing the iteration, iterateListMin() returns the new Alpha and    ****
% **** Beta - values                                                                ****
% ****                                                                              ****
% **** example :                                                                    ****
% ****  iterateListMax(2,[b2c3,b3c4],Break,10000,-10000,NewAlpha,NewBeta).          ****
% **************************************************************************************
iterateListMin(_,[],false,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta).
iterateListMin(Depth,[Turn|TurnList],Break,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta) :-
  %shift(Depth),write('DoMinTurn : '), write(Turn), write('\n'),
  doTurn(Turn),
  NewDepth is Depth - 1,
  max(NewDepth,PreviousAlpha,PreviousBeta,_,TempNewBeta1),
  %shift(Depth),write('UndoMinTurn : '), write(Turn), write('\n'),
  checkEvaluationIsBestMin(TempNewBeta1,PreviousAlpha,PreviousBeta,TempNewAlpha2,TempNewBeta2,Break,Depth,Turn),
  undoTurn(Turn),
  (Break = true,NewBeta = TempNewAlpha2, NewAlpha = TempNewBeta2 ; 
   iterateListMin(Depth,TurnList,_,TempNewAlpha2,TempNewBeta2,NewAlpha,NewBeta)).

% **************************************************************************************
% ***** swapAlphaBeta(Depth,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta) *****
% **************************************************************************************
% **** swapAlphaBeta swaps the order of Alpha and Beta, if Depth != topLevelDepth   ****    
% ****                                                                              ****
% **** example :                                                                    ****
% ****  swapAlphaBeta(2,10,3,NewAlpha,NewBeta).                                     ****
% **************************************************************************************
swapAlphaBeta(Depth,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta) :- topLevelDepth(Depth).
swapAlphaBeta(_,PreviousAlpha,PreviousBeta,PreviousBeta,PreviousAlpha).

% *******************************************************************************************************
% ** checkEvaluationIsBestMax(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,Break,Depth,Turn) **
% *******************************************************************************************************
% **** checkEvaluationIsBestMax checks if Evaluation is new Alpha value.                              ****    
% **** executes checkBreakIteration() and returns Break                                              ****
% ****                                                                                               ****
% **** example :                                                                                     ****
% ****  checkEvaluationIsBestMax(10,5,20,NewAlpha,NewBeta,Break,3,b2c3).                             ****
% *******************************************************************************************************
checkEvaluationIsBestMax(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,Break,Depth,Turn) :-
  Evaluation > PreviousAlpha,
  NewAlpha = Evaluation,
  NewBeta = PreviousBeta,
  checkBreakIteration(NewAlpha,NewBeta,Break),  
  checkSaveTurn(Depth,Turn).

checkEvaluationIsBestMax(_,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta,false,_,_).

% *******************************************************************************************************
% ** checkEvaluationIsBestMin(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,Break,Depth,Turn) **
% *******************************************************************************************************
% **** checkEvaluationIsBestMin checks if Evaluation is new Beta value.                              ****    
% **** executes checkBreakIteration() and returns Break                                              ****
% ****                                                                                               ****
% **** example :                                                                                     ****
% ****  checkEvaluationIsBestMin(10,20,5,NewAlpha,NewBeta,Break,3,b2c3).                             ****
% *******************************************************************************************************
checkEvaluationIsBestMin(Evaluation,PreviousAlpha,PreviousBeta,NewAlpha,NewBeta,Break,Depth,Turn) :-
  Evaluation < PreviousBeta,
  NewBeta = Evaluation,
  NewAlpha = PreviousAlpha,
  checkBreakIteration(NewAlpha,NewBeta,Break),
  checkSaveTurn(Depth,Turn).

checkEvaluationIsBestMin(_,PreviousAlpha,PreviousBeta,PreviousAlpha,PreviousBeta,false,_,_).

% *********************************************************
% ********* checkBreakIteration(Alpha,Beta,Break) *********
% *********************************************************
% **** checkBreakIteration returns "true"              ****    
% **** if Beta =< Alpha                                ****
% ****                                                 ****
% **** example :                                       ****
% ****    checkBreakIteration(2,3,Break).              ****
% *********************************************************
checkBreakIteration(Alpha,Beta,true) :- 
  Beta =< Alpha.
  %write('Break because of beta('), write(Beta), write(')=<alpha('), write(Alpha), write(')\n').
checkBreakIteration(_,_,false).

% *********************************************************
% *************** checkSaveTurn(Depth,Turn) ***************
% *********************************************************
% **** saves turn, if Depth == topLevelDepth           ****    
% ****                                                 ****
% **** example :                                       ****
% ****    checkBreakIteration(2,c2d3).                 ****
% *********************************************************
checkSaveTurn(Depth,Turn) :- topLevelDepth(Depth), retractall(bestTurn(_)), asserta(bestTurn(Turn)).
checkSaveTurn(_,_). % save nothing if depth != topLevelDepth

% *********************************************************
% *********** generateList(Color,PossibleTurns) ***********
% *********************************************************
% **** generates a list of all possible turns          ****    
% ****                                                 ****
% **** example :                                       ****
% ****    generateList(s,PossibleTurns).               ****
% *********************************************************
generateList(Color,PossibleTurns) :- 
  calcPossibleTurnsForColor(Color,PossibleTurns).
  %write('generated list : '), write(PossibleTurns), write('\n').

% *********************************************************
% ********************** shift(Count)**********************
% *********************************************************
% **** writes out Count blanks                         ****    
% ****                                                 ****
% **** example :                                       ****
% ****    shift(5).                                    ****
% *********************************************************
shift(Count) :- topLevelDepth(Depth), Temp is Depth - Count, Temp == 0.
shift(Count) :- write(' '), NewCount is Count + 1, shift(NewCount). 

% *********************************************************
% ******************* evaluate(Result) ********************
% *********************************************************
% **** returns a evaluation of the current board       ****    
% ****                                                 ****
% **** example :                                       ****
% ****    evaluate(Result).                            ****
% *********************************************************
evaluate(Result) :- rateBoard(Result).
