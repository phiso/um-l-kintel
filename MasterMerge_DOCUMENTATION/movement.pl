%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Beispiel der Zugbewegungen-Routinen für UM-L´
%
% U. Meyer, Okt. 2008, Feb. 2015
%
%
% Benötigt board.pl
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
:- dynamic
	fehler/2.
fehler(nein,weiss).	% Schwarz beginnt das Spiel, s.u.!!

:- [board].
:- [alphaBeta].

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%% BEGIN-NEW-CODE %%%%%%%%%%%%%%%%%%%%%

% #################### MISCELLANEOUS ######################

% *********************************************************
% ******************** member(M,List) *********************
% *********************************************************
% **** Proofs if M is inside List                      ****
% ****                                                 ****
% **** example :                                       ****
% ****    member(4,[1,2,a,b,4]).                       ****
% *********************************************************

member(M,[M|_]).
member(M,[_|NewList]) :- member(M,NewList).

% *********************************************************
% ************ concatLists(List1,List2,Result) ************
% *********************************************************
% **** Concats List1 and List2 and returns them into   ****
% **** Result.                                         ****
% ****                                                 ****
% **** example :                                       ****
% ****    concatLists([a1,a2,a3],[b1,b2,b3,b4],R).     ****
% *********************************************************

concatLists([],Result,Result).
concatLists([Head|Body],List2,Result) :- concatLists(Body,[Head|List2],Result).

% *********************************************************
% ******** splitFromLast(List,AllExceptLast,Last) *********
% *********************************************************
% **** Returns last element of a list as Last and all  ****
% **** other elements as AllExceptLast.                ****
% ****                                                 ****
% **** example :                                       ****
% ****    splitFromLast([a,b,c,d],AllOther,Last).      ****
% *********************************************************

splitFromLast(List,AllExceptLast,Last) :- last(List,Last), delete(List,Last,AllExceptLast).


% #################### HASH-FUNCTION ######################

% *********************************************************
% ********************* hash(Result) **********************
% *********************************************************
% **** hash(Result) calculates a hash value of the     ****
% **** current situation on the board and returns it   ****
% **** into Result.                                    ****
% **** Every situation on the board can be identified  ****
% **** by its hash-value.                              ****
% **** This function uses getCoefficientForColor()     ****
% **** ATTENTION:                                      ****
% ****  hash(Result) works on a List of all fields of  ****
% ****  the board. For performance optimization use    ****
% ****  hash(FieldList,Result), because this does not  ****
% ****  work on all fields of the board, but only on   ****
% ****  the ones, which are inside FieldList.          ****
% ****                                                 ****
% **** example :                                       ****
% ****    hash(Result).                                ****
% ****    hash([a4,a6,b3,b5],Result).                  ****
% *********************************************************


hash(Result) :- hash([a4,a6,b3,b5,b7,c2,c4,c6,c8,d1,d3,d5,d7,d9,e2,e4,e6,e8,f3,f5,f7,g4,g6],0,Result).
hash(FieldList,Result) :- hash(FieldList,0,Result).

hash([],Result,Result).
hash([Field|ListOfFields],Temp,Result) :- brett(Field,[]),
                                          hash(ListOfFields,Temp,Result).
hash([Field|ListOfFields],Temp,Result) :- brett(Field,[Color|_]), fieldNumber(Field,FieldNumber),
			                  getCoefficientForColor(Color,Coefficient), 
				          NewTemp is Temp + (Coefficient * 10**FieldNumber),
				          hash(ListOfFields,NewTemp,Result).

% *********************************************************
% ****** getCoefficientForColor(Color,Coefficient) ********
% *********************************************************
% **** getCoefficientForColor returns a Coefficient for****
% **** a given Color. This is needed in hash()         ****
% ****                                                 ****
% **** example :                                       ****
% ****    getCoefficientForColor(s,C).                 ****
% *********************************************************
getCoefficientForColor(s,1).
getCoefficientForColor(w,2).
getCoefficientForColor(g,3).
getCoefficientForColor(r,4).

% *********************************************************
% ************** fieldNumber(Field,Number) ****************
% *********************************************************
% **** fieldNumber returns a the index of the field    ****
% **** if you count them from left to right and down   ****
% **** to up                                           ****
% ****                                                 ****
% **** example :                                       ****
% ****    fieldNumber(a4,N).                           ****
% *********************************************************
fieldNumber(a4,1).
fieldNumber(a6,2).
fieldNumber(b3,3).
fieldNumber(b5,4).
fieldNumber(b7,5).
fieldNumber(c2,6).
fieldNumber(c4,7).
fieldNumber(c6,8).
fieldNumber(c8,9).
fieldNumber(d1,10).
fieldNumber(d3,11).
fieldNumber(d5,12).
fieldNumber(d7,13).
fieldNumber(d9,14).
fieldNumber(e2,15).
fieldNumber(e4,16).
fieldNumber(e6,17).
fieldNumber(e8,18).
fieldNumber(f3,19).
fieldNumber(f5,20).
fieldNumber(f7,21).
fieldNumber(g4,22).
fieldNumber(g6,23).

% #################### PROGRAM_FLOW #######################

% *********************************************************
% ********************* start(color) **********************
% *********************************************************
% **** start(Color) is an alternative function to      ****
% **** start(). It automatically calculates and inserts****
% **** a turn if Color has to make his turn.           ****
% **** If its not Color who has to make his turn, the  ****
% **** user has to enter the turn into the keyboard,   ****
% **** as it was in original function start().         ****
% ****                                                 ****
% **** example :                                       ****
% ****    start(schwarz).                              ****
% *********************************************************


start(schwarz) :- start(schwarz,weiss).
start(weiss)   :- start(weiss,schwarz).

start(AIColor,NonAIColor) :-  set_prolog_stack(local, limit(5 000 000 000)),
				retractall(currentColor(_,_)),
                 assertz(currentColor(weiss)),
	         retractall(aiColor(_)),
        	 assertz(aiColor(AIColor)),
	         retractall(nonAiColor(_)),
        	 assertz(nonAiColor(NonAIColor)),                 
                 initBrett,
                 retract(fehler(_,_)),
                 assertz(fehler(nein,weiss)),
	         asserta(rating(s, 90)), asserta(rating(w, 90)),
		 asserta(usedTime(0)),
                 dialog.
          

% *********************************************************
% *********************** dialog() ************************
% *********************************************************
% **** dialog() is a replacement of the orignal        ****
% **** function dialog(). The original function is now ****
% **** called dialogOld() and is no longer used.       ****
% **** dialog() has exactly the same functionality as  ****
% **** dialogOld(), but it automatically calculates    ****
% **** and inserts a turn, if Color (which was set     ****
% **** by start(Color)) has to turn.                   ****
% **** If its not this Color which has to make its     ****
% **** turn, the user has to enter the next turn into  ****
% **** the keyboard, as it was in dialogOld().         ****
% **** dialog() uses getNextTurn(...) for calculating  ****
% **** and setting a turn.                             ****
% ****                                                 ****
% **** example :                                       ****
% ****    dialog.                                      ****
% *********************************************************
dialog :-  color(NextColor),
           currentColor(PreviousColor),
           (NextColor \== PreviousColor;fail),
           retractall(currentColor(_)),
           assertz(currentColor(NextColor)),
           schreibeBrett,
           write(NextColor),
           write(' am Zug  ==> '),
           calcPossibleTurnsForColor(NextColor,Result),
           write('Moegliche Zuege : '),write(Result),
           write('\n ==> '),
	       aiColor(AIColor),
           getNextTurn(AIColor,NextColor,Zugfolge),
		   usedTime(Time),
		   write('\n vergangene Zeit: '), write(Time), write('\n'),
           ziehen(NextColor,Zugfolge),
		   rating(s, ValueS),
		   rating(w, ValueW),
           write('Bewertung des Brettes fuer S: '), write(ValueS), write('\n'),
           write('Bewertung des Brettes fuer W: '), write(ValueW), write('\n'),
           dialog.

% *********************************************************
% ********************* color(Result) *********************
% *********************************************************
% **** alternative function to farbe, which calculates ****
% **** alternating schwarz and weiß.                   **** 
% ****                                                 ****
% **** example :                                       ****
% ****    color(Color),                                ****
% ****    currentColor(CurrentColor),                  ****
% ****    (Color \== CurrentColor;fail),               ****
% ****    retractall(currentColor(_)),                 ****
% ****    assertz(currentColor(Color)),                ****
% *********************************************************

color(schwarz).
color(weiss).


% ********************************************************* 
% ****** getNextTurn(AIColor,CurrentColor,NextTurn) ******* 
% ********************************************************* 
% **** calls the actual alpha-beta via calcnextturn    ****
% **** to compute the best next turn for the given     ****
% **** colorand adds up the time that was needed       ****
% **** to compute                                      ****
% ****                                                 ****
% **** example :                                       ****
% ****    getNextTurn(schwarz,schwarz,NextTurn)        **** 
% *********************************************************

getNextTurn(AIColor,AIColor,NextTurn) :- 
	get_time(TimeBefore),
		calcNextTurn(8,NextTurn),
	get_time(TimeAfter),
	retract(usedTime(Time)),
	NewTime is Time + (TimeAfter - TimeBefore),
	asserta(usedTime(NewTime)),
	write(NextTurn).

getNextTurn(_,_,NextTurn) :- read(NextTurn).
%getNextTurn(_,_,NextTurn) :- read(NextTurn).



% ********************************************************* 
% ****************** calcPositionList() ******************* 
% ********************************************************* 
% **** calcPositionList() creates and asserts two lists**** 
% **** L1 and L2 within the asserted facts             ****
% **** positionList(w,L1) and positionList(s,L2)       **** 
% **** L1 is a list of all fields, where w is on top.  **** 
% **** L2 is a list of all fields, where s is on top.  ****
% **** This can be used for evaluation and to not need ****
% **** iterate all fields during calcPossibleTurns().  ****
% ****                                                 **** 
% **** example :                                       **** 
% ****    calcPositionList.                            **** 
% ****    positionList(w,L1).                          ****
% ****    positionList(s,L2).                          ****
% **** example :                                       ****
% ****    calcPositionList(w).			       ****	
% ****    positionList(w,L).                           ****
% *********************************************************

calcPositionList :- retractall(positionList(_,_)), calcPositionList(w), calcPositionList(s).
calcPositionList(Color) :- retractall(positionList(Color,_)),calcPositionList(Color,[a4,a6,b3,b5,b7,c2,c4,c6,c8,d1,d3,d5,d7,d9,e2,e4,e6,e8,f3,f5,f7,g4,g6],[]).
calcPositionList(Color,[],Result) :- assertz(positionList(Color,Result)).
calcPositionList(Color,[Field|FieldList],PositionList) :- brett(Field,[Color|_]),calcPositionList(Color,FieldList,[Field|PositionList]).
calcPositionList(s,[Field|FieldList],PositionList) :- brett(Field,[r|_]),calcPositionList(s,FieldList,[Field|PositionList]).
calcPositionList(w,[Field|FieldList],PositionList) :- brett(Field,[g|_]),calcPositionList(w,FieldList,[Field|PositionList]).
calcPositionList(Color,[_|FieldList],PositionList) :- calcPositionList(Color,FieldList,PositionList).

% *********************************************************
% ******** calcPossibleTurnsForColor(Color,Result) ********
% *********************************************************
% **** Calculates the currently possible turns for     ****
% **** Color and returns them into Result.             ****
% ****                                                 ****
% **** For to speed up this function, calcPositionList ****
% **** must not be called every time again, the        ****
% **** program has to store a current positionList     ****
% **** and has to keep it up to date at every move and ****
% **** jump. This allows the program to not always     ****
% **** iterate all fields and search for positions.    ****
% ****                                                 ****
% **** example :                                       ****
% ****    calcPossibleTurnsForColor(schwarz,Result)    ****
% *********************************************************

calcPossibleTurnsForColor(schwarz,Result) :- calcPossibleTurnsForColor(s,Result).
calcPossibleTurnsForColor(weiss,Result) :- calcPossibleTurnsForColor(w,Result).

calcPossibleTurnsForColor(Color,Result) :- calcPositionList(Color), positionList(Color,PositionList),                                          
                                           calcPossibleTurnsForColor(jump,PositionList,PositionList,[],Result).

calcPossibleTurnsForColor(jump,[],FullPositionList,[],Result):-  calcPossibleTurnsForColor(move,FullPositionList,[],Result).
calcPossibleTurnsForColor(jump,[],_,Result,Result).

                                                                

calcPossibleTurnsForColor(jump,[Position|PositionList],FullPositionList,TurnsList,Result):- not(calcPossibleJumps(Position)),!,
                                                                      possibleJumps(Position,Jumps),
                                                                      mergePositionAndTurns(Position,Jumps,MergedJumps),
                                                                      concatLists(MergedJumps,TurnsList,ConcatedList),
                                                                      calcPossibleTurnsForColor(jump,PositionList,FullPositionList,ConcatedList,Result).

calcPossibleTurnsForColor(move,[],[],[]).                                                                     

calcPossibleTurnsForColor(move,[],Result,Result).                                                                      
calcPossibleTurnsForColor(move,[Position|PositionList],TurnsList,Result):- not(calcPossibleMoves(Position)),!,                                                                      
                                                                      possibleMoves(Position,Moves),
                                                                      mergePositionAndTurns(Position,Moves,MergedMoves),
                                                                      concatLists(MergedMoves,TurnsList,ConcatedList),
                                                                      calcPossibleTurnsForColor(move,PositionList,ConcatedList,Result).
% *********************************************************
% *** mergePositionAndTurns(Position,TurnsList,Result) ****
% *********************************************************
% **** merges the given turnslist with the position    ****
% **** to create a list of complete turns (e.g. d3e4)  ****
% **** from the list of given positions (e.g. [e4,...])****
% *********************************************************

mergePositionAndTurns(Position,TurnsList,Result) :- mergePositionAndTurns(Position,TurnsList,[],Result).
mergePositionAndTurns(_,[],Result,Result).
mergePositionAndTurns(Position,[Turn|TurnsList],MergedList,Result) :- concat(Position,Turn,MergedItem),
                                                                      mergePositionAndTurns(Position,TurnsList,[MergedItem|MergedList],Result).
% *********************************************************
% **************** calcPossibleMoves(Field) ***************
% *********************************************************
% **** Creates a list of all possible moves, from      ****
% **** Field and adds this list to the asserted fact   ****
% **** possibleTurns(Field,List)                       ****
% ****                                                 ****
% **** example :                                       ****
% ****    calcPossibleMoves(e4)                        ****
% *********************************************************


calcPossibleMoves(Field) :- retractall(possibleMoves(Field,_)),
                            assertz(possibleMoves(Field,[])),
                            (move(Field,Turn);move(Turn,Field)),
                            brett(Turn,[]),
                            checkPromotionAllowsMove(Field,Turn),
                            possibleMoves(Field,Temp),
		                        retract(possibleMoves(Field,_)),
                            assertz(possibleMoves(Field,[Turn|Temp])),
                            fail.

% *********************************************************
% **************** calcPossibleJumps(Field) ***************
% *********************************************************
% **** Creates a list of all possible jumps, from      ****
% **** Field and adds this list to the asserted fact   ****
% **** possibleTurns(Field,List)                       ****
% ****                                                 ****
% **** example :                                       ****
% ****    calcPossibleJumps(e4)                        ****
% *********************************************************

calcPossibleJumps(Field) :- retractall(possibleJumps(Field,_)),
                            assertz(possibleJumps(Field,[])),
                            (jump(Field,Middle,Turn);jump(Turn,Middle,Field)), % get all theoretically possible jumps
                            brett(Turn,[]),            % check if new field is empty
                            checkPromotionAllowsJump(Field,Turn), 
                            brett(Field,[Color|_]),    % getCurrentColor
                            brett(Middle,[Oppo|_]),    % check if the color you want to jail is your opponent
                            opponent(Color,Oppo),      %    -||-
                            possibleJumps(Field,Temp),
                            retract(possibleJumps(Field,_)),
                            assertz(possibleJumps(Field,[Turn|Temp])),
                            fail.


% *********************************************************
% ****** checkPromotionAllowsMove(FromField,ToField) ****** 
% *********************************************************
% **** Checks if the color on fromField, depending on  **** 
% **** his promotion theoretically is allowed to move  ****
% **** in the direction of FromField to ToField.       ****
% **** It does NOT! check, if the move is possible.    ****
% **** TODO : promotion kicks promotion                ****
% ****                                                 ****
% **** example :                                       ****
% ****    checkPromotionAllowsMove(e4,d5)              ****
% *********************************************************

checkPromotionAllowsMove(From,To) :- brett(From,[s|_]), move(To,From).
checkPromotionAllowsMove(From,To) :- brett(From,[r|_]), (move(To,From);move(From,To)).
checkPromotionAllowsMove(From,To) :- brett(From,[w|_]), move(From,To).
checkPromotionAllowsMove(From,To) :- brett(From,[g|_]), (move(From,To);move(To,From)).  

% *********************************************************
% ****** checkPromotionAllowsJump(FromField,ToField) ******
% *********************************************************
% **** Checks if the color on fromField, depending on  ****
% **** his promotion theoretically is allowed to jump  ****
% **** in the direction of FromField to ToField.       ****
% **** It does NOT! check, if the jump is possible.    ****
% **** TODO : promotion kicks promotion                ****
% ****                                                 ****
% **** example :                                       ****
% ****    checkPromotionAllowsJump(e4,c6)              ****
% *********************************************************

checkPromotionAllowsJump(From,To) :- brett(From,[s|_]), jump(To,_,From).
checkPromotionAllowsJump(From,To) :- brett(From,[r|_]), (jump(To,_,From);jump(From,_,To)).
checkPromotionAllowsJump(From,To) :- brett(From,[w|_]), jump(From,_,To).
checkPromotionAllowsJump(From,To) :- brett(From,[g|_]), (jump(From,_,To);jump(To,_,From)).

% *********************************************************
% ************** calcNextTurn(Color,Result) ***************
% *********************************************************
% **** calls the alpha-beta algorithm with given depth ****
% ****                                                 ****
% **** example:                                        ****
% ****    calcNextTurn(8,Result).                      ****
% *********************************************************

calcNextTurn(Depth,NextTurn) :- alphaBeta(Depth,_,NextTurn).

% ################# DO_TURN-METHODS ####################

% NOTES : for to speed up evaluation, evaluate only one
%         color, not both


% *********************************************************
% ******* doTurn(From,To) *******
% *********************************************************
% **** extracts the source- and target-position from   ****
% **** the given turn string, then checks if its a     ****
% **** move or a jump and performs that                ****
% **** example :                                       ****
% **** 		doTurn(e4d3).                              ****
% *********************************************************

doTurn(Turn) :-  sub_atom(Turn,0,2,_,From),
	         sub_atom(Turn,2,2,_,To),
		 saveTurn(From,To),
		 doTurn(From,To).
                 %schreibeBrett. % !!!!!!!!!TODO
		 
doTurn(From,To) :- (move(From,To);move(To,From)), doMove(From,To).
doTurn(From,To) :- (jump(From,M,To);jump(To,M,From)), 
	           brett(M,[Oppo|Jailed]),
                   brett(From,[HeadFrom|_]),
                   opponent(HeadFrom,Oppo),
                   doJump(From,M,Oppo,Jailed,To).

 
						    

% ################## UNDO_TURN-METHODS ####################

% *********************************************************
% ******************* saveTurn(From,To) *******************
% *********************************************************
% **** saves the given turn with a assert to assure    ****
% **** that its possible to undo it later.             ****
% ****												   ****
% **** example :                                       ****
% **** 		saveTurn(e4d3).                            ****
% *********************************************************

saveTurn(From,To) :- (move(From,To);move(To,From)),
                     brett(From,FromColors), brett(To,ToColors), rating(s,ValueS), rating(w,ValueW),
                     asserta(savedTurn(From,FromColors,To,ToColors, ValueS, ValueW)).

saveTurn(From,To) :- (jump(From,Middle,To);jump(To,Middle,From)),
                     brett(From,FromColors), brett(Middle,MiddleColors), brett(To,ToColors), rating(s,ValueS), rating(w,ValueW),
                     asserta(savedTurn(From,FromColors,Middle,MiddleColors,To,ToColors,ValueS,ValueW)).

% *********************************************************
% ******************* undoTurn(From,To) *******************
% *********************************************************
% **** revokes the given turn and brings the board     ****
% **** back to the old state. Also removes the assert  ****
% **** from saveTurn.                                  ****
% ****												   ****
% **** example :                                       ****
% **** 		undoTurn(e4d3).                            ****
% *********************************************************

undoTurn(Turn) :- sub_atom(Turn,0,2,_,From),
                  sub_atom(Turn,2,2,_,To),
                  undoTurn(From,To). 
                  %schreibeBrett. % !!!!!!!!!!!!!!!!TODO


undoTurn(From,To) :- (move(From,To);move(To,From)), % was move not jump
                     retract(brett(From,_)), retract(brett(To,_)), retractall(rating(_,_)),
                     retract(savedTurn(From,FromColors,To,ToColors, ValueS, ValueW)),
                     asserta(brett(From,FromColors)),asserta(brett(To,ToColors)), asserta(rating(s, ValueS)), asserta(rating(w, ValueW)).

undoTurn(From,To) :- (jump(From,Middle,To);jump(To,Middle,From)),
                     retract(brett(From,_)), retract(brett(Middle,_)), retract(brett(To,_)), retractall(rating(_,_)),
                     retract(savedTurn(From,FromColors,Middle,MiddleColors,To,ToColors,ValueS,ValueW)),
                     asserta(brett(From,FromColors)), asserta(brett(Middle,MiddleColors)), asserta(brett(To,ToColors)), asserta(rating(s,ValueS)), asserta(rating(w,ValueW)).
   

%undoTurn(From,To,FromWasAlreadyPromotion)

%undoTurn(From,To,true) :- (move(From,To);move(To,From)), % was move not jump
%                          brett(To,ColorsAtTo), % get Lists at From and at To
%                          retract(brett(From,_)), retract(brett(To,_)), % delete old values
%                          asserta(brett(From,ColorsAtTo)), asserta(brett(To,[])).

%undoTurn(From,To,false) :- (move(From,To);move(To,From)), % was move not jump
%                           brett(To,[ColorsAtToHead|ColorsAtToTail]), % get Lists at From and at To
%                           promotion(To,_,_),                             % if we have to undo a promotion
%                           degradiere(ColorsAtToHead,DegradedColorAtTo), % undo promotion
%			   retract(brett(From,_)), retract(brett(To,_)), % delete old values
%                           asserta(brett(From,[DegradedColorAtTo|ColorsAtToTail])), asserta(brett(To,[])).

%undoTurn(From,To,false) :- (move(From,To);move(To,From)), % was move not jump
%                           brett(To,ColorsAtTo), % get Lists at From and at To
%                           not(promotion(To,_,_)), % we don't have to undo a promotion
%                           retract(brett(From,_)), retract(brett(To,_)), % delete old values
%                           asserta(brett(From,ColorsAtTo)), asserta(brett(To,[])).                    
                     
%undoTurn(From,To,true)  :- (jump(From,Middle,To);jump(To,Middle,From)), % was jump not move
%                           brett(Middle,ColorsAtMiddle), brett(To,ColorsAtTo), % get Lists at From and at To
%                           retract(brett(From,_)), retract(brett(Middle,_)), retract(brett(To,_)), % delete old values
%                           splitFromLast(ColorsAtTo,AllExceptTheLast,Last),
%                           asserta(brett(From,AllExceptTheLast)), asserta(brett(Middle,[Last|ColorsAtMiddle])), asserta(brett(To,[])).

%undoTurn(From,To,false)  :- (jump(From,Middle,To);jump(To,Middle,From)), % was jump not move
%                            brett(Middle,ColorsAtMiddle), brett(To,[ColorsAtToHead|ColorsAtToTail]), % get Lists at From and at To
%			    promotion(To,_,_),                             % if we have to undo a promotion
%                            degradiere(ColorsAtToHead,DegradedColorAtTo), % undo promotion
%                            retract(brett(From,_)), retract(brett(Middle,_)), retract(brett(To,_)), % delete old values
%                            splitFromLast([ColorsAtToHead|ColorsAtToTail],[_|AllExceptTheLastAndHead],Last),
%                            asserta(brett(From,[DegradedColorAtTo|AllExceptTheLastAndHead])), asserta(brett(Middle,[Last|ColorsAtMiddle])), asserta(brett(To,[])).

%undoTurn(From,To,false)  :- (jump(From,Middle,To);jump(To,Middle,From)), % was jump not move
%                            brett(Middle,ColorsAtMiddle), brett(To,ColorsAtTo), % get Lists at From and at To
%                            not(promotion(To,_,_)), % we don't have to undo a promotion
%                            retract(brett(From,_)), retract(brett(Middle,_)), retract(brett(To,_)), % delete old values
%                            splitFromLast(ColorsAtTo,AllExceptTheLast,Last),
%                            asserta(brett(From,AllExceptTheLast)), asserta(brett(Middle,[Last|ColorsAtMiddle])), asserta(brett(To,[])).

% #################### RATING-METHODS #####################

% *********************************************************
% ******************* rateBoard(Result) *******************
% *********************************************************
% **** rateBoard() rates the Board based on the        ****
% **** current aiColor using rating(s,X) and           ****
% **** rating(w,Y)                                     ****
% ****                                                 ****
% **** example:                                        ****
% ****		rateBoard(Result).                         ****
% ****		rateBoard(Color, Result).                  ****
% *********************************************************

rateBoard(Result):-
	aiColor(Color),
	rateBoard(Color, Result).

rateBoard(schwarz, Result):-
	rating(s, ValueS), rating(w, ValueW),
	Result is ValueS - ValueW.

rateBoard(weiss, Result):-
	rating(s, ValueS), rating(w, ValueW),
	Result is ValueW - ValueS.

% *********************************************************
% ******** recalcBoardState(Old, [O|[JH|JT]], New) ********
% *********************************************************
% **** Recalculates the board state based on the board ****
% **** before and the changes made in the turn and     ****
% **** asserts these changes for black and white:      ****
% **** 		rating(s,X).                               ****
% **** 		rating(w,Y).                               ****
% **** Uses value() to get certain values of f.e.      ****
% **** towers or stones                                ****
% ****                                                 ****
% **** example:										   ****
% **** 		recalcBoardState([w], [s], [w,s]).         ****
% *********************************************************

value([w], 10).
value([s], 10).
value([r], 20).
value([g], 20).
value([w|_], 15).
value([s|_], 15).
value([r|_], 25).
value([g|_], 25).
value([], 0).

emptyList([]).

recalcBoardState(Old, [O|J], New) :-
		(O == s; O == r),
		emptyList(J),
		retract(rating(s,ValueS)),
		value([O], ValueLossOfS),!,
		NewValueS is ValueS - ValueLossOfS,
		asserta(rating(s,NewValueS)),
		value(Old, ValueLossOfW),!,
		value(New, ValueGainOfW),!,
		retract(rating(w,ValueW)),
		NewValueW is ValueW - ValueLossOfW + ValueGainOfW,
		asserta(rating(w,NewValueW)).
		
recalcBoardState(Old, [O|J], New) :-
		(O == w; O == g),
		emptyList(J),
		retract(rating(w,ValueW)),
		value([O], ValueLossOfW),!,
		NewValueW is ValueW - ValueLossOfW,
		asserta(rating(w,NewValueW)),
		value(Old, ValueLossOfS),!,
		value(New, ValueGainOfS),!,
		retract(rating(s,ValueS)),
		NewValueS is ValueS - ValueLossOfS + ValueGainOfS,
		asserta(rating(s,NewValueS)).

recalcBoardState(Old, [O, JH|JT], New) :-
		(O == s; O == r),
		JH == s,
		retract(rating(s,ValueS)),
		value([O], ValueLossOfS),!,
		value([JH|JT], ValueGainOfS),!,
		NewValueS is ValueS - ValueLossOfS + ValueGainOfS,
		asserta(rating(s,NewValueS)),
		value(Old, ValueLossOfW),!,
		value(New, ValueGainOfW),!,
		retract(rating(w,ValueW)),
		NewValueW is ValueW - ValueLossOfW + ValueGainOfW,
		asserta(rating(w,NewValueW)).
		
recalcBoardState(Old, [O, JH|JT], New) :-
		(O == s; O == r),
		JH == w,
		retract(rating(s,ValueS)),
		value([O], ValueLossOfS),!,
		value([JH|JT], AdditionalValueGainOfW),!,
		NewValueS is ValueS - ValueLossOfS,
		asserta(rating(s,NewValueS)),
		value(Old, ValueLossOfW),!,
		value(New, ValueGainOfW),!,
		retract(rating(w,ValueW)),
		NewValueW is ValueW - ValueLossOfW + ValueGainOfW + AdditionalValueGainOfW,
		asserta(rating(w,NewValueW)).
		
recalcBoardState(Old, [O, JH|JT], New) :-
		(O == w; O == g),
		JH == w,
		retract(rating(w,ValueW)),
		value([O], ValueLossOfW),!,
		value([JH|JT], ValueGainOfW),!,
		NewValueW is ValueW - ValueLossOfW + ValueGainOfW,
		asserta(rating(w,NewValueW)),
		value(Old, ValueLossOfS),!,
		value(New, ValueGainOfS),!,
		retract(rating(s,ValueS)),
		NewValueS is ValueS - ValueLossOfS + ValueGainOfS,
		asserta(rating(s,NewValueS)).
		
recalcBoardState(Old, [O, JH|JT], New) :-
		(O == w; O == g),
		JH == s,
		retract(rating(w,ValueW)),
		value([O], ValueLossOfW),!,
		value([JH|JT], AdditionalValueGainOfS),!,
		NewValueW is ValueW - ValueLossOfW,
		asserta(rating(w,NewValueW)),
		value(Old, ValueLossOfS),!,
		value(New, ValueGainOfS),!,
		retract(rating(s,ValueS)),
		NewValueS is ValueS - ValueLossOfS + ValueGainOfS + AdditionalValueGainOfS,
		asserta(rating(s,NewValueS)).
	
recalcBoardState(X,X):-!.	
recalcBoardState(OldHead, NewHead):-
		retract(rating(OldHead, Value)),
		value([OldHead], ValueLoss),!,
		value([NewHead], ValueGain),!,
		NewValue is Value - ValueLoss + ValueGain,
		asserta(rating(OldHead, NewValue)).


% %%%%%%%%%%%%%%%%%%%%%% END-NEW-CODE %%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Mögliche Züge aus der Sicht von Weiss (von -- nach)
move(a4,b3).
move(a4,b5).
move(a6,b5).
move(a6,b7).
move(b3,c2).
move(b3,c4).
move(b5,c4).
move(b5,c6).
move(b7,c6).
move(b7,c8).
move(c2,d1).
move(c2,d3).
move(c4,d3).
move(c4,d5).
move(c6,d5).
move(c6,d7).
move(c8,d7).
move(c8,d9).
move(d1,e2).
move(d3,e2).
move(d3,e4).
move(d5,e4).
move(d5,e6).
move(d7,e6).
move(d7,e8).
move(d9,e8).
move(e2,f3).
move(e4,f3).
move(e4,f5).
move(e6,f5).
move(e6,f7).
move(e8,f7).
move(f3,g4).
move(f5,g4).
move(f5,g6).
move(f7,g6).

% Mögliche Sprünge aus der Sicht von Weiss (von -- über -- nach)
jump(a4,b3,c2).
jump(a4,b5,c6).
jump(a6,b5,c4).
jump(a6,b7,c8).
jump(b3,c2,d1).
jump(b3,c4,d5).
jump(b5,c4,d3).
jump(b5,c6,d7).
jump(b7,c6,d5).
jump(b7,c8,d9).
jump(c2,d3,e4).
jump(c4,d3,e2).
jump(c4,d5,e6).
jump(c6,d5,e4).
jump(c6,d7,e8).
jump(c8,d7,e6).
jump(d1,e2,f3).
jump(d3,e4,f5).
jump(d5,e4,f3).
jump(d5,e6,f7).
jump(d7,e6,f5).
jump(d9,e8,f7).
jump(e2,f3,g4).
jump(e4,f5,g6).
jump(e6,f5,g4).
jump(e8,f7,g6).

start :-
	retract(fehler(_,_)),
	assertz(fehler(nein,weiss)),
	initBrett,
        dialog.
        
dialogOld :-
	farbe(Farbe),
	schreibeBrett,
	write(Farbe),
	write(' am Zug  ==> '),
	read(Zugfolge),
	ziehen(Farbe,Zugfolge).
farbe(F) :- fehler(ja,F).
farbe(schwarz) :- fehler(nein,weiss).
farbe(weiss) :- fehler(nein,schwarz).
farbe(F) :- farbe(F).

ziehen(_,halt) :-
        write('Spiel beendet.'),nl,
        halt.
ziehen(_,init) :-
        write('Alles auf Anfang!'),nl,
        start.
ziehen(Farbe,Zug) :-
	atom_length(Zug,4),
	zug(Farbe,Zug),
	retract(fehler(_,_)),asserta(fehler(nein,Farbe)). %,
	%!,fail.
ziehen(Farbe,_) :-
	nl,write('Ungültige Eingabe!'),nl,nl,
	retract(fehler(_,_)),asserta(fehler(ja,Farbe)),
	fail.
zug(Farbe,Zug) :-
	sub_atom(Zug,0,2,_,FeldA),
	sub_atom(Zug,2,2,_,FeldZ),
	FeldA \== FeldZ,
	selbst(Farbe,FeldA,Kopf),  % ist das Ausgangsfeld mit einer eigenen Farbe besetzt?
	brett(FeldZ,[]),           % ist das Zielfeld leer?
	test(Kopf,FeldA,FeldZ),
	!.

selbst(schwarz,Feld,s) :-
	brett(Feld,[s|_]).
selbst(schwarz,Feld,r) :-
	brett(Feld,[r|_]).
selbst(weiss,Feld,w) :-
	brett(Feld,[w|_]).
selbst(weiss,Feld,g) :-
	brett(Feld,[g|_]).

test(s,A,Z) :-
        jump(Z,M,A),
        brett(M,[Oppo|Jailed]),
        opponent(s,Oppo),
        doJump(A,M,Oppo,Jailed,Z).
test(s,A,Z) :-
        move(Z,A),
        doMove(A,Z).
test(w,A,Z) :-
        jump(A,M,Z),
        brett(M,[Oppo|Jailed]),
        opponent(w,Oppo),
        doJump(A,M,Oppo,Jailed,Z).
test(w,A,Z) :-        
        move(A,Z),
        doMove(A,Z).

% ATTENTION NEW CODE FOR PROMOTION MOVEMENT
test(r,A,Z) :-
        (jump(Z,M,A);jump(A,M,Z)),
        brett(M,[Oppo|Jailed]),
        opponent(r,Oppo),
        doJump(A,M,Oppo,Jailed,Z).

test(r,A,Z) :- 
        (move(Z,A);move(A,Z)),
        doMove(A,Z).

test(g,A,Z) :- 
       (jump(Z,M,A);jump(A,M,Z)),
        brett(M,[Oppo|Jailed]),
        opponent(g,Oppo),
        doJump(A,M,Oppo,Jailed,Z).

test(g,A,Z) :- 
        (move(Z,A);move(A,Z)),
        doMove(A,Z).



opponent(g,r).
opponent(r,g).
opponent(g,s).
opponent(r,w).
% ATTENTION NEW CODE FOR PROMOTION MOVEMENT

opponent(w,s).
opponent(w,r).
opponent(s,w).
opponent(s,g).

degradiere(g,w).
degradiere(r,s).
degradiere(X,X).     % degradiere nicht

% Felder, auf denen gewöhnliche Steine befördert werden
promotion(a4,s,r).
promotion(a6,s,r).
promotion(g4,w,g).
promotion(g6,w,g).
promotion(_,X,X).    % befördere nicht

doJump(X,M,O,J,Y) :-
        retract(brett(X,[Kopf|S])),
        assertz(brett(X,[])),
        retract(brett(M,_)),
        assertz(brett(M,J)),
        retract(brett(Y,_)),
        promotion(Y,Kopf,Offz),
        degradiere(O,G),
        append([Offz|S],[G],New),
        assertz(brett(Y,New)),
		get_time(TimeBefore),
			recalcBoardState([Kopf|S], [O|J], New), % Added Codeline
		get_time(TimeAfter),
		retract(usedTime(Time)),
		NewTime is Time + (TimeAfter - TimeBefore),
		asserta(usedTime(NewTime)).
doMove(X,Y) :-
        retract(brett(X,[Kopf|S])),
        assertz(brett(X,[])),
        retract(brett(Y,_)),
        promotion(Y,Kopf,Offz),
        assertz(brett(Y,[Offz|S])),
		get_time(TimeBefore),
			recalcBoardState(Kopf, Offz), % Added Codeline
		get_time(TimeAfter),
		retract(usedTime(Time)),
		NewTime is Time + (TimeAfter - TimeBefore),
		asserta(usedTime(NewTime)).
        
demo :-
	schreibeBrett(schwarz),write(e3d4),nl,
	selbst(schwarz,e3,KopfS1),
	e3 \== d4,
	testMove(schwarz,KopfS1,e3,d4),
	move(e3,d4),
	schreibeBrett(weiss),write(c5e3),nl,
	selbst(weiss,c5,KopfW),
	c5 \== e3,
	testJump(weiss,KopfW,c5,e3,M1),
	jump(c5,M1,e3),
	schreibeBrett(schwarz),write(f2d4),nl,
	selbst(schwarz,f2,KopfS2),
	f2 \== d4,
	testJump(schwarz,KopfS2,f2,d4,M2),
	jump(f2,M2,d4),
	schreibeBrett(weiss),!.
