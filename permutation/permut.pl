permut(L) :- permutation(L,NewL),
             write('possiblePermutation('),
             write(NewL), write(').'),write('\n'),fail.

permutWriteToFile(L,Path) :-
  open(Path,write,Stream),
  not(permutWriteToFile2(L,Stream)),
  close(Stream).


permutWriteToFile2(L,Stream) :-   
  permutation(L,NewL),
  not(possiblePermutation(NewL)),
  asserta(possiblePermutation(NewL)),
  write(Stream,'possiblePermutation('),
  write(Stream,NewL),
  write(Stream,').'),
  write(Stream,'\n'),
  fail.

permutUML :-
set_prolog_stack(local, limit(5 000 000 000)),
permutWriteToFile([[s],[s],[s],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[w],[w],[w]],'testUML.txt').
%permutWriteToFile([s,s,w,s],'testUML.txt')
%permutWriteToFile([s,s,w],'testUML.txt').

perms_R([], _).
perms_R([Item|NList], List):-
  member(Item, List),
  perms_R(NList, List).